import React from 'react'
import Header from './components/Header';
import 'bootstrap/dist/css/bootstrap.min.css';
import Mail from './components/Mail';

function App() {
  return (
    <div className="app">
      <Header />
      <Mail />
    </div>
  )
}

export default App