import React from 'react';
import { useEffect, useState } from "react";
import axios from "axios";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

function Mail() {
  const [data, setData] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:3000/data/data.json")
      .then(res => {
        setData(res.data)
      })
      .catch(function (err) {
      })
  }, [setData]);

  function sentThroughFormatter(column) {
    return (
      <div>
        {column.text} <i class="fa-solid fa-circle-info"></i>
      </div>
    );
  }
  function sentThroughDataFormatter(cell) {
    const iconDiv = cell.length > 0 && cell.map(element => <i className={element} />
    )
    return (
      <div>
        {iconDiv}
      </div>
    );
  }
  const columns = [
    {
      dataField: 'title',
      text: 'TITLE'
    },
    {
      dataField: 'message',
      text: 'MESSAGE'
    },
    {
      dataField: 'sentBy',
      text: 'SENT BY',
    },
    {
      dataField: 'sentThrough',
      text: 'SENT THROUGH',
      headerFormatter: sentThroughFormatter,
      formatter: sentThroughDataFormatter,
    },
    {
      dataField: 'dateCreated',
      text: 'DATE CREATED'
    },
    {
      dataField: 'startDate',
      text: 'START DATE'
    },
    {
      dataField: 'endDate',
      text: 'END DATE'
    }
  ];

  const customTotal = (from, to, size) => (
    <div className="react-bootstrap-table-pagination-total">
      Items per page
      <div className='results d-flex justify-content-end'>
        Showing {from} to {to} of {size}
      </div>
    </div>
  );

  const options = {
    paginationSize: 4,
    pageStartIndex: 1,
    firstPageText: 'First',
    prePageText: 'Back',
    nextPageText: 'Next',
    lastPageText: 'Last',
    nextPageTitle: 'First page',
    prePageTitle: 'Pre page',
    firstPageTitle: 'Next page',
    lastPageTitle: 'Last page',
    showTotal: true,
    paginationTotalRenderer: customTotal,
    disablePageTitle: true,
    sizePerPageList: [{
      text: '10', value: 10
    }]
  };
  return (
    <>
      <div className='announcements d-flex justify-content-between'>
        Announcements
        <button className='post'>
          Post an Announcement
        </button>
      </div>
      <div className='description'>
        View, create, or edit announcements for all employees of your company.
      </div>
      <div className='table__main'>
        <BootstrapTable keyField='id' data={data} columns={columns} pagination={paginationFactory(options)} bordered={false} />
      </div>
    </>

  )
}

export default Mail