import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import '../css/Header.css';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';

function Header() {
    return (
        <div className='nav__header'>
            <Navbar bg="dark" variant={"dark"} expand="lg" className="nav__black">
                <Container fluid>
                    <Navbar.Brand href="#">
                        <img src='https://sprout.ph/wp-content/uploads/2021/08/Sprout-Logo-1-02-1.png' alt="Sprout Solutions Logo" className='nav__logo'></img>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse className="navbar__collapse">
                        <Nav className="mx-auto">
                        </Nav>
                        <Nav className="d-flex justify-content-flex-end navbar__main">
                            <NavDropdown title="My Requests" className="navbar__requests">
                                <NavDropdown.Item href="#physicalexam">Annual Physical Exam</NavDropdown.Item>
                                <NavDropdown.Item href="#townhalls">Townhall Session</NavDropdown.Item>
                                <NavDropdown.Item href="#vacationleave">Vacation Leave</NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown title="My Team" className="navbar__team">
                                <NavDropdown.Item href="#newhires">New Hires</NavDropdown.Item>
                                <NavDropdown.Item href="#documents">
                                    Pending Documents
                                </NavDropdown.Item>
                                <NavDropdown.Item href="#outgoingemployee">
                                    Outgoing Employee
                                </NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown title="Company" className="navbar__company">
                                <NavDropdown.Item href="#company1">Company 1</NavDropdown.Item>
                                <NavDropdown.Item href="#company2">
                                    Company 2
                                </NavDropdown.Item>
                                <NavDropdown.Item href="#company3">
                                    Company 3
                                </NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown title="Administration Tools" className="navbar__admintools">
                                <NavDropdown.Item href="#tool1">Tool 1</NavDropdown.Item>
                                <NavDropdown.Item href="#tool2">
                                    Tool 2
                                </NavDropdown.Item>
                                <NavDropdown.Item href="#tool3">
                                    Tool 3
                                </NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown title="Maintenance" className="navbar__maintenance">
                                <NavDropdown.Item href="#maintenance1">Maintenance 1</NavDropdown.Item>
                                <NavDropdown.Item href="#maintenance2">
                                    Maintenance 2
                                </NavDropdown.Item>
                                <NavDropdown.Item href="#maintenance3">
                                    Maintenance 3
                                </NavDropdown.Item>
                            </NavDropdown>
                            <Form className="search__bar">
                                <Form.Control
                                    type="text"
                                    placeholder="Employee Search"
                                    aria-label="Search"
                                />
                            </Form>
                        </Nav>
                        <div>
                            <FontAwesomeIcon icon="fa-solid fa-magnifying-glass" size="lg" className='fa__icon' inverse />
                            <FontAwesomeIcon icon="fa-solid fa-bell" size="lg" className='fa__icon2' inverse />
                            <FontAwesomeIcon icon="fa-solid fa-circle" size="lg" className='fa__icon3' inverse />
                        </div>
                        <NavDropdown title="Admin" className="navbar__admin" variant={"dark"}>
                            <NavDropdown.Item href="#accountsettings">Account Settings</NavDropdown.Item>
                            <NavDropdown.Item href="#profile">
                                Profile
                            </NavDropdown.Item>
                            <NavDropdown.Item href="#logout">
                                Logout
                            </NavDropdown.Item>
                        </NavDropdown>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}

export default Header